package dao.pojo;

import java.io.Serializable;

/**
 * Created by admin on 11.03.2016.
 */
public class Client implements Serializable {
	private int kodkli;
	private String name;
	private String groupName;

	public Client() {}

	public Client(int kodkli) {
		setKodkli(kodkli);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

  public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public int getKodkli() {
		return kodkli;
	}

	public void setKodkli(int kodkli) {
		this.kodkli = kodkli;
	}

	@Override
	public int hashCode() {
		int result = getKodkli() * 7 << 2;
		result = 31 * result + (getName() != null ? getName().hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "Client{@" + hashCode() +
				" kodkli=" + kodkli +
				", name='" + name + '\'' +
				'}';
	}
}
