package dao.pojo;

import java.io.Serializable;

/**
 * Created by admin on 11.03.2016.
 */
public class KodM implements Serializable {
	private Client client;
	private String inv;
	private String serial;
	private String inner;


	public KodM() {
		setClient(new Client());
	}

	public KodM(Client cl) {
		setClient(cl);
	}

	public String getInv() {
		return inv != null ? inv : "";
	}

	public void setInv(String inv) {
		this.inv = inv;
	}

	public String getSerial() {
		return serial != null ? serial : "";
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public String getInner() {
		return inner != null ? inner : "";
	}

	public void setInner(String inner) {
		this.inner = inner;
	}

	@Override
	public String toString() {
		return "KodM{" +
				"name='" + getClient().getName() + '\'' +
				", kodm=" +getClient().getKodkli() +
				", inv='" + inv + '\'' +
				", serial='" + serial + '\'' +
				", inner='" + inner + '\'' +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		KodM kodM = (KodM) o;

		if (getClient().getKodkli() != kodM.getClient().getKodkli()) return false;
		if (!getClient().getName().equals(kodM.getClient().getName())) return false;
		if (!getInv().equals(kodM.getInv())) return false;
		if (!getSerial().equals(kodM.getSerial())) return false;
		return getInner().equals(kodM.getInner());

	}

	@Override
	public int hashCode() {
		int result = getClient().getName().hashCode();
		result = 31 * result + getClient().getKodkli();
		result = 17 * result + getInv().hashCode();
		result = 31 * result + getSerial().hashCode();
		result = 7 * result + getInner().hashCode();
		return result;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}
}
