package dao.pojo.doc;

import dao.pojo.KodM;

import java.io.Serializable;

public class ContentDoc implements Serializable {
	private KodM kodM;
	private double quant;
	private double sum;

	private String comment;

	public KodM getKodM() {
		return kodM;
	}

	public void setKodM(KodM kodM) {
		this.kodM = kodM;
	}

	public double getQuant() {
		return quant;
	}

	public void setQuant(double quant) {
		this.quant = quant;
	}

	public double getSum() {
		return sum;
	}

	public void setSum(double sum) {
		this.sum = sum;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@Override
	public String toString() {
		return "ContentDoc{" +
				"kodM=" + kodM +
				", quant=" + quant +
				", sum=" + sum +
				", comment='" + comment + '\'' +
				'}';
	}
}
