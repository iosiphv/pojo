package dao.pojo.doc;

import dao.pojo.Client;
import dao.pojo.KodM;

import java.util.function.BiFunction;

public class ContentWaterDoc extends ContentDoc {
	private double ost;
	private boolean iskredit;

	public ContentWaterDoc() {
		super();
	}

	public ContentWaterDoc(Client client, Double sum) {
		super();
		super.setKodM(new KodM(client));
		super.setSum(sum);
	}

	public boolean isIskredit() {
		return ost < 0;
	}

	public double getOst() {
		return ost;
	}

	public void setOst(double ost) {
		this.ost = ost;
	}

	@Override
	public String toString() {
		return super.toString() + "ContentWaterDoc{" +
				"kodM=" + super.getKodM() +
				",ost=" + ost +
				'}';
	}
}
