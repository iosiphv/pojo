package dao.pojo.doc;

/**
 * Created by admin on 11.03.2016.
 */
public interface Doc {

	enum TYPE_TRANSFER {
		EMPTY, MOVEMENT, REPAIR, WRITE_OFF, REFILL, NEW;

		public static TYPE_TRANSFER get(int t) {
			TYPE_TRANSFER r;
			switch (t) {
				case 1:
					r = MOVEMENT;
					break;
				case 2:
					r = REPAIR;
					break;
				case 3:
					r = WRITE_OFF;
					break;
				case 4:
					r = REFILL;
					break;
				case 5:
					r = NEW;
					break;
				default:
					r = EMPTY;
					break;
			}
			return r;
		}
	}
}
