package dao.pojo.doc;

import dao.pojo.Client;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

/**
 * Created by admin on 11.03.2016.
 */
public class IncomeDoc implements Doc, Serializable {
	private int koddoc;
	private Date date;
	private int num;
	private Client income;
	private Client outgone;
	private TYPE_TRANSFER typeOfTransfer;

	private List<ContentDoc> contentDocList;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public Client getIncome() {
		return income;
	}

	public void setIncome(Client income) {
		this.income = income;
	}

	public Client getOutgone() {
		return outgone;
	}

	public void setOutgone(Client outgone) {
		this.outgone = outgone;
	}

	public TYPE_TRANSFER getTypeOfTransfer() {
		return typeOfTransfer;
	}

	public void setTypeOfTransfer(TYPE_TRANSFER typeOfTransfer) {
		this.typeOfTransfer = typeOfTransfer;
	}

	public List<ContentDoc> getContentDocList() {
		return contentDocList;
	}

	public void setContentDocList(List<ContentDoc> contentDocList) {
		this.contentDocList = contentDocList;
	}

	public int getKoddoc() {
		return koddoc;
	}

	public void setKoddoc(int koddoc) {
		this.koddoc = koddoc;
	}

	@Override
	public String toString() {
		return "IncomeDoc{" +
				"koddoc=" + koddoc +
				", date=" + date +
				", num='" + num + '\'' +
				", income=" + income +
				", outgone=" + outgone +
				", typeOfTransfer=" + typeOfTransfer +
				", contentDocList=" + contentDocList +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		IncomeDoc incomeDoc = (IncomeDoc) o;

		return getKoddoc() == incomeDoc.getKoddoc();

	}

	@Override
	public int hashCode() {
		return getKoddoc();
	}
}
