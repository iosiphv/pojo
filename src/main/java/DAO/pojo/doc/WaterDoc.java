package dao.pojo.doc;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

/**
 * Created by ovt10 on 11.01.2017.
 */
public class WaterDoc implements Serializable, Doc {
	private int koddoc;
	private Date data;
	private Long summa;
	private List<ContentWaterDoc> contentDocList;


	public int getKoddoc() {
		return koddoc;
	}

	public void setKoddoc(int koddoc) {
		this.koddoc = koddoc;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Long getSumma() {
		return summa;
	}

	public void setSumma(Long summa) {
		this.summa = summa;
	}

	public List<ContentWaterDoc> getContentDocList() {
		return contentDocList;
	}

	public void setContentDocList(List<ContentWaterDoc> contentDocList) {
		this.contentDocList = contentDocList;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder
				.append("waterDoc {")
				.append(koddoc).append(",")
				.append(data.toString()).append(",")
				.append(summa).append("}");
		return builder.toString();
	}

	@Override
	public boolean equals(Object obj) {
		boolean ret = false;
		if ((obj instanceof WaterDoc) & (obj != null)) {
			WaterDoc tObj = (WaterDoc) obj;
			ret = Objects.equals(tObj.getKoddoc(), getKoddoc())
					& Objects.equals(tObj.getSumma(), getSumma())
					& Objects.equals(tObj.getData(), getData());
		}
		return ret;
	}

	public void setNewWaterDoc(WaterDoc doc) {

	}
}
